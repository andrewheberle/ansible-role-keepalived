#!/bin/sh
# {{ ansible_managed }}

# Check args
[ $# -ne 1 ] && exit 1

# Variables
RUNDIR="/run/keepalived"
STATE_FILE="$(printf '%s/%s_INSTANCE.state' "${RUNDIR}" "$1")"

# Check file exists
[ -f "${STATE_FILE}" ] || exit 1

# Are we not MASTER
grep -q MASTER "${STATE_FILE}" || exit 1

# Got here so we are OK
exit 0
