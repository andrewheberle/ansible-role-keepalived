#!/bin/sh
# {{ ansible_managed }}
#
# SSL connect check
echo | openssl s_client -connect {{ script_template_args.host|default("localhost") }}:{{ script_template_args.port|default(443) }} >/dev/null 2>&1 || exit 1
