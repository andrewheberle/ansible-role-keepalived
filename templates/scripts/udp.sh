#!/bin/sh
# {{ ansible_managed }}
#
# UDP health check
nc -u -z {{ script_template_args.host|default("localhost") }} {{ script_template_args.port|default(7) }} || exit 1
