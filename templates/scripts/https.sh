#!/bin/sh
# {{ ansible_managed }}
#
# HTTPS check
printf 'GET {{ script_template_args.url|default("/healthcheck.htm") }} {{ script_template_args.http_version|default("HTTP/1.1") }}\r\nHost: {{ script_template_args.hostname|default(ansible_hostname) }}\r\nConnection: Close\r\n\r\n' | \
openssl s_client -quiet -connect {{ script_template_args.host|default("localhost") }}:{{ script_template_args.port|default(443) }} 2>/dev/null | \
head -1 | \
grep -q -e "^HTTP/.* {{ script_template_args.response|default(200) }}" || exit 1
