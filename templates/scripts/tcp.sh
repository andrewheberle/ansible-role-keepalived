#!/bin/sh
# {{ ansible_managed }}
#
# TCP health check
nc -z {{ script_template_args.host|default("localhost") }} {{ script_template_args.port|default(7) }} || exit 1
