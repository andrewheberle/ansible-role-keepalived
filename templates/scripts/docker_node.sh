#!/bin/sh
# {{ ansible_managed }}
#
# Docker node health check
[ "$(docker node inspect --format {{ '{{' }}.Status.State{{ '}}' }} self)" = "ready" ] && exit 0 || exit 1
