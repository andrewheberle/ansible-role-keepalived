#!/bin/sh
# Ansible managed

usage() {
    printf 'Usage: %s [-c <response>] [-s <http|https>] [-p <port>] [-u <URL to check>] -h <host>\n\n' "$0"
}

isindrainmode() {
    URL=$1
    CODE=$2
    RESPONSE=$(curl --connect-timeout 2 -I -s "${URL}" | head -1 | cut -d' ' -f2)
    if [ "${RESPONSE}" = "${CODE}" ]; then
        # OK Code was matched so NOT in drain mode
        return 1
    else
        # No matched so we are in drain mode
        return 0
    fi

}
# Defaults
OKCODE=200
SCHEME=http
CHECKPATH=/
PORT=80

# Start by assuming failure
EXITCODE=1

# Parse arguments
while getopts 'c:s:u:p:h:' c; do
    case $c in
        c) OKCODE=$OPTARG ;;
        s) SCHEME=$OPTARG ;;
        p) PORT=$OPTARG ;;
        u) CHECKPATH=$OPTARG ;;
        h) HOST=$OPTARG ;;
        *)
            printf 'Unknown option "%s"\n\n' "$c"
            usage
            exit 1
        ;;
    esac
done

# Make sure we got a host provided
if [ -z "${HOST}" ]; then
    printf 'Error: No host provided\n\n'
    usage
    exit 1
fi

if isindrainmode "${SCHEME}://${HOST}:${PORT}${CHECKPATH}" "${OKCODE}"; then
    EXITCODE=2
else
    EXITCODE=3
fi

exit ${EXITCODE}
