#!/bin/sh
# Ansible Managed
RUNDIR="/run/keepalived"

# Args
TYPE=$1
NAME=$2
STATE=$3

# Create state dir if required
[ -d "${RUNDIR}" ] || mkdir -p "${RUNDIR}" || exit 1

# Create temp file
TEMP_FILE="$(mktemp "${RUNDIR}/.${NAME}_${TYPE}.XXXXXX")"

# Output state
if printf '%s\n' "${STATE}" > "${TEMP_FILE}"; then
    # Move into place
    mv -f "${TEMP_FILE}" "${RUNDIR}/${NAME}_${TYPE}.state"
fi

# Remove temp file
[ -f "${TEMP_FILE}" ] && rm -f "${TEMP_FILE}"
