#!/bin/sh
(
cat <<EOF
module local 1.0;

require {
        type keepalived_t;
        class capability { setgid setuid };
}

#============ keepalived_t ==============

allow keepalived_t self:capability setgid;
allow keepalived_t self:capability setuid;
EOF
) > local.te
yum install -y checkpolicy policycoreutils-python && \
checkmodule -M -m -o local.mod local.te && \
semodule_package -o local.pp -m local.mod && \
semodule -i local.pp
