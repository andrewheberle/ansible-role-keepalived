#!/bin/sh
# Ansible Managed
#
# UDP virtual_server MISC_CHECK
[ $# -ne 2 ] && exit 2
HOST=$1
PORT=$2
if [ "$3" = "" ]; then
    SEND_STRING="default send string"
else
    SEND_STRING=$3
fi

# Send data and return response
echo "${SEND_STRING}" | nc -u -z -w 16 "${HOST}" "${PORT}" || exit 1
