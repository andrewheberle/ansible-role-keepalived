#!/bin/sh
# Ansible Managed
if [ "$1" = "-n" ]; then
	shift
	DRYRUN="on"
else
	unset DRYRUN
fi

#SCOPE=$1
#NAME=$2
ENDSTATE=$3
IP=$4
PRIO=$5

case $ENDSTATE in
	"FAULT")
		if [ -n "${DRYRUN}" ]; then
			echo /opt/gobgp/bin/gobgp global rib del "${IP}"
		else
			/opt/gobgp/bin/gobgp global rib del "${IP}"
		fi
	;;
	"MASTER"|"BACKUP")
		# MASTER and BACKUP are treated the same
		if [ -z "${PRIO}" ]; then
			PRIO="100"
		fi
		if [ -n "${DRYRUN}" ]; then
			echo /opt/gobgp/bin/gobgp global rib add "${IP}" med "${PRIO}"
		else
			/opt/gobgp/bin/gobgp global rib add "${IP}" med "${PRIO}"
		fi
	;;
	*)
		echo "Invalid state"
		exit 1
	;;
esac
