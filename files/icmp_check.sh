#!/bin/sh
# Ansible Managed
#
# ICMP virtual_server MISC_CHECK
[ $# -ne 1 ] && exit 2
HOST=$1

# Ping host and return response
ping -c 2 "${HOST}" || exit 1
